//class declaration below.

class AppInfo {
String appname;
String appseccar;
String appdeveloper;
int appyear;

//function declaration below to //change appname to uppercase
//letters only

void transform () {
appname= appname.toUpperCase();
}

//function to display this class //object's data below.

@override toString() {
return ('$appname - $appseccar - $appdeveloper - $year \n');
}
//new line tag above moves a //cursor to the next line after //printing all the data on the current //line.
}

void main() {

//Creation of a new AppInfo class //object instance below

AppInfo appinfo = new AppInfo ();

//appinfo is our object's name as //declared above.

//assignment of data to an object's //properties or variables.

appinfo.appname= "Checkers Hyper Group";
appinfo.appseccar = "Best Innovation App";
appinfo.appdeveloper = "Checkers Hyper Dev Team";
appinfo.appyear = "2022";

//printing of not yet changed data of //an object (appname in this case).

print (appinfo.toString());

//function call for transforming all //appname letters to uppercase //letters only.

transform ();

//printing of object's data with all //appname letters changed to //uppercase letters.

print(appinfo.toString());

}
